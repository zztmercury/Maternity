package com.chml.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * CORSFilter for remote debugging 16-9-1.
 */
public class CORSFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        ((HttpServletResponse)response).addHeader("Access-Control-Allow-Origin","*");
        ((HttpServletResponse)response).addHeader("Access-Control-Allow-Headers","X-Requested-With, Content-Type,access_token");
        ((HttpServletResponse)response).addHeader("Access-Control-Allow-Methods","GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH");
        chain.doFilter(request,response);
    }

    @Override
    public void destroy() {

    }
}
