package com.chml.service;

import com.chml.pojo.MediaData;

/**
 * Created by zzt on 16-10-6.
 */
public interface MediaDataService {
    String getIntroVideoUrl(int mid);
}
