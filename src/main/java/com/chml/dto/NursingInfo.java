package com.chml.dto;

import java.util.Date;
import java.util.List;

/**
 * 月嫂活力信息格式
 */
public class NursingInfo {
    Date first_time;
    int num_of_family;
    int num_of_twins;
    int experience;
    List<String> location;
    List<String> family_type;

    public Date getFirst_time() {
        return first_time;
    }

    public void setFirst_time(Date first_time) {
        this.first_time = first_time;
    }

    public int getNum_of_family() {
        return num_of_family;
    }

    public void setNum_of_family(int num_of_family) {
        this.num_of_family = num_of_family;
    }

    public int getNum_of_twins() {
        return num_of_twins;
    }

    public void setNum_of_twins(int num_of_twins) {
        this.num_of_twins = num_of_twins;
    }

    public List<String> getFamily_type() {
        return family_type;
    }

    public void setFamily_type(List<String> family_type) {
        this.family_type = family_type;
    }

    public List<String> getLocation() {
        return location;
    }

    public void setLocation(List<String> location) {
        this.location = location;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }
}
