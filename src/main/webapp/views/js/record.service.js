/**
 * Created by zzt on 16-10-4.
 */
'use strict';
angular.module('services').factory('RecordService', function ($resource, globalVar,$cookies) {
    return $resource("", {},
        {
            addRecord: {
                method: 'POST',
                url: globalVar.backendUrl + "/yuesao/:mid/record",
                params: {mid: '@mid'},
                headers: {access_token: $cookies.get('access_token')}
            },
            getRecords: {
                method: 'GET',
                url: globalVar.backendUrl + "/yuesao/:mid/records"
            },
            getServingMonthsInYear:{
                method: 'GET',
                url: globalVar.backendUrl + "/yuesao/:mid/record/:year/months",
                params: {mid:'@mid',year:'@year'}
            },
            getRecordsByMonth: {
                method: 'GET',
                url: globalVar.backendUrl + "/yuesao/:mid/records/:year/:month",
                params: {mid:'@mid',year:'@year',month:'@month'}
            }
        });
});