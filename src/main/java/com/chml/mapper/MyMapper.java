package com.chml.mapper;

import com.chml.dto.MaternityInfoDTO;
import com.chml.params.MaternityListParam;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by zzt on 16-9-21.
 */
public interface MyMapper {
    float getAvgGrade(int mid);

    List<MaternityInfoDTO> selectMaternityInfoByFilter(@Param("param") MaternityListParam param, @Param("offset") int offset, @Param("name") String name);

    int countMaternityInfoByFilter(@Param("param") MaternityListParam param, @Param("name") String name);

    List<Integer> selectServingMonthsInYear(@Param("mid") int mid, @Param("year") int year);

    List<String> selectMonthsBefore(@Param("mid") int mid, @Param("date") Date date);

    List<String> selectMonthsAfter(@Param("mid") int mid, @Param("date") Date date);

}
