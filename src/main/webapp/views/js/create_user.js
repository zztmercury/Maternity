var app = angular.module('myApp', []);
app.controller('createUserController', function ($scope,$http) {
    $scope.createUser = function () {
        var account = $scope.account;
        var password = $scope.new_password;
        var rpwd = $scope.rpassword;
        if (password!=rpwd) {
            alert(password+" "+rpwd);
            alert("两次输入密码不一致");
            return;
        }
        var accessToken = $.cookie("access_token");
        var uid = sessionStorage.getItem("uid");
        $http({
            method:"post",
            url: ip + "user",
            data:{
                uid:uid,
                access_token:accessToken,
                account:account,
                nickname:account,
                password:password
            }
        }).success(function (response) {
            if (response.success) {
                alert("新用户创建成功");
                location.href = 'overview_page.html';
            }
            else {
                alert(response.msg);
                if(data.msg == "登录状态已失效,请重新登录"){
                    sessionStorage.clear();
                    $.cookie("access_token", null,{path:"/"});
                    location.href = "login.html";
                }
            }
        })
    };

    $scope.pageBack = function () {
        location.href = 'overview_page.html';
    };
});