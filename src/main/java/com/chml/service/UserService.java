package com.chml.service;

import com.chml.pojo.User;

/**
 * 操作数据库用户信息的一些接口
 */
public interface UserService {
    User Login(String account, String password) throws Exception;
    int ChangePassword(int uid, String old_password, String new_password, String access_token) throws Exception;
    boolean isValidRequest(int uid,String accessToken) throws Exception;

    boolean isValidRequest(String accessToken);

    int createUser(String account, String password, String nickname) throws Exception;

    User getUserByToken(String token);
}
