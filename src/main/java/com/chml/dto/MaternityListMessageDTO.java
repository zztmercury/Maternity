package com.chml.dto;

import java.util.List;

/**
 * 获取月嫂列表时的返回信息格式
 */
public class MaternityListMessageDTO {
    int total_count;
    int records_count;
    List<MaternityInfoDTO> records;

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public int getRecords_count() {
        return records_count;
    }

    public void setRecords_count(int records_count) {
        this.records_count = records_count;
    }

    public List<MaternityInfoDTO> getRecords() {
        return records;
    }

    public void setRecords(List<MaternityInfoDTO> records) {
        this.records = records;
    }
}
