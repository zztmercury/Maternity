package com.chml.params;

/**
 * 获取月嫂详细信息时的传入数据格式
 */
public class GetMaternityDetailParam {
    int uid;
    String access_token;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}
