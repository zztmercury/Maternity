package com.chml.service;

import com.chml.dto.GradePointDto;
import com.chml.dto.ServiceRecordDto;
import com.chml.exception.FailCallException;
import com.chml.pojo.Evaluation;

import java.util.List;

/**
 * Created by zzt on 16-9-30.
 */
public interface EvaluationService {
    /**
     * 获取月嫂在对应评分下的评价总数
     * @param mid 月嫂id
     * @param grade 评分
     * @return 月嫂在对应评分下的评价总数
     */
    int getGradeCount(int mid, int grade);

    /**
     * 获取月嫂评价总数
     * @param mid 月嫂id
     * @return 月嫂评价总数
     */
    int getGradeCount(int mid);

    /**
     * 删除月嫂评价
     * @param mid 月嫂id
     * @return 影响的记录数
     */
    int deleteByMid(int mid);

    /**
     * 添加服务记录（评价）
     * @param mid 月嫂id
     * @param recordParam 服务记录信息
     * @return 插入后的id
     */
    int addRecord(int mid, ServiceRecordDto recordParam) throws FailCallException;

    /**
     * 根据id获取单条服务记录（评价）
     * @param gid 服务记录id
     * @param mid 月嫂id
     * @return 对应id服务记录信息，当gid为null时返回月嫂的新评价
     */
    Evaluation getRecord(Integer gid, int mid);

    /**
     * 根据月嫂id获取服务记录（评价）
     * @param mid 月嫂id
     * @param page
     * @param perPage
     * @param order
     * @return
     */
    List<ServiceRecordDto> getRecordsByMid(int mid, int page, int perPage, String order);

    List<GradePointDto> getGradePointsByMid(int mid);

    List<Integer> getServingMonthsInYear(int mid, int year);

    List<ServiceRecordDto> getRecordsByMonth(int mid, int year, int month);

    String prevServingMonth(int mid, int year, int month);

    String nextServingMonth(int mid, int year, int month);
}
