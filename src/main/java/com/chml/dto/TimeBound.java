package com.chml.dto;

import java.util.Date;

/**
 * 时间起始格式
 */
public class TimeBound {
    Date start;
    Date end;

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
