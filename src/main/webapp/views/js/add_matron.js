/*
 * 显示用户名字
 * */
var addMaterialImgTemplate;
var loadBody = function () {
    var username = sessionStorage.getItem("maternity_name");
    if (username == null) {
        alert("请先登录");
        location.href = "login.html";
    }
    else {
        document.getElementById("maternity_name").innerHTML = sessionStorage.getItem("maternity_name");
    }
    // 绑定上传配置
    bind_upload();
    addMaterialImgTemplate = $('div.material-img:last').prop('outerHTML');
    $.datepicker.setDefaults($.datepicker.regional['zh-CN']);
    $('.date-picker').datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true
    });
};

/*
 * 登出
 * */
var logout = function () {
    sessionStorage.clear();
    $.cookie("access_token", null, {path: "/"});
    location.href = "login.html";
};

var app = angular.module('myApp', []);
app.controller('myCtrl', function ($scope, $compile, $http) {

    // 加载省市数据
    $scope.provinces = cityList;
    $scope.changeProvince = function () {
        $scope.cities = $scope.province.c;
    };

    //删除证书
    $scope.deleteYyyImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("yyy_img").src = "img/upload-img.png";
            document.getElementById("yyy_img").removeAttribute('data-url');
            $scope.yyy_serial = "";
            $scope.yyy_level_select = "";
            document.getElementById('yyy').style.display = "none";
        }
    };
    $scope.deleteJzfwyImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("jzfwy_img").src = "img/upload-img.png";
            document.getElementById("jzfwy_img").removeAttribute('data-url');
            $scope.jzfwy_serial = "";
            $scope.jzfwy_level_select = "";
            document.getElementById('jzfwy').style.display = "none";
        }
    };
    $scope.deleteYypcyImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("yypcy_img").src = "img/upload-img.png";
            document.getElementById("yypcy_img").removeAttribute('data-url');
            $scope.yypcy_serial = "";
            $scope.yypcy_level_select = "";
            document.getElementById('yypcy').style.display = "none";
        }
    };
    $scope.deleteByyImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("byy_img").src = "img/upload-img.png";
            document.getElementById("byy_img").removeAttribute('data-url');
            $scope.byy_serial = "";
            $scope.byy_level_select = "";
            document.getElementById('byy').style.display = "none";
        }
    };
    $scope.deleteMyhlsImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("myhls_img").src = "img/upload-img.png";
            document.getElementById("myhls_img").removeAttribute('data-url');
            $scope.myhls_serial = "";
            document.getElementById('myhls').style.display = "none";
        }
    };
    $scope.deleteYysImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("yys_img").src = "img/upload-img.png";
            document.getElementById("yys_img").removeAttribute('data-url');
            $scope.yys_serial = "";
            document.getElementById('yys').style.display = "none";
        }
    };
    $scope.deleteHsbyzImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("hsbyz_img").src = "img/upload-img.png";
            document.getElementById("hsbyz_img").removeAttribute('data-url');
            $scope.hsbyz_serial = "";
            document.getElementById('hsbyz').style.display = "none";
        }
    };
    $scope.deleteCrsImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("crs_img").src = "img/upload-img.png";
            document.getElementById("crs_img").removeAttribute('data-url');
            $scope.crs_serial = "";
            document.getElementById('crs').style.display = "none";
        }
    };
    $scope.deleteMyjkzdsImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("myjkzds_img").src = "img/upload-img.png";
            document.getElementById("myjkzds_img").removeAttribute('data-url');
            $scope.myjkzds_serial = "";
            document.getElementById('myjkzds').style.display = "none";
        }
    };

    //证书上传
    $scope.setNoneDisplay = function () {
        document.getElementById('yyy').style.display = "none";
        document.getElementById('jzfwy').style.display = "none";
        document.getElementById('yypcy').style.display = "none";
        document.getElementById('byy').style.display = "none";
        document.getElementById('myhls').style.display = "none";
        document.getElementById('yys').style.display = "none";
        document.getElementById('hsbyz').style.display = "none";
        document.getElementById('crs').style.display = "none";
        document.getElementById('myjkzds').style.display = "none";
    };
    $scope.setNoneDisplay();
    $scope.setSelectedCertification = function (x) {
        if (x == "myhls") {
            document.getElementById('myhls').style.display = "inline-block";
        }
        else if (x == "yyy") {
            document.getElementById('yyy').style.display = "inline-block";
        }
        else if (x == "jzfwy") {
            document.getElementById('jzfwy').style.display = "inline-block";
        }
        else if (x == "yypcy") {
            document.getElementById('yypcy').style.display = "inline-block";
        }
        else if (x == "byy") {
            document.getElementById('byy').style.display = "inline-block";
        }
        else if (x == "hsbyz") {
            document.getElementById('hsbyz').style.display = "inline-block";
        }
        else if (x == "crs") {
            document.getElementById('crs').style.display = "inline-block";
        }
        else if (x == "yys") {
            document.getElementById('yys').style.display = "inline-block";
        }
        else if (x == "myjkzds") {
            document.getElementById('myjkzds').style.display = "inline-block";
        }
    };

    $scope.addMatron = function () {
        //获取uid以及access_token
        var access_token = $.cookie("access_token");
        var uid = sessionStorage.getItem("uid");

        var hy;
        var sy;
        var xl = "";
        var hljy;
        var yy = [];
        var ywcpx = [];
        var ggyy = [];
        var hljt = [] ;

        var name = $scope.name;
        var age = $scope.age;
        var phone = $scope.phone_num;
        var emergency = $scope.emergency;
        var relation = $scope.relation;
        var contact = $scope.contact;
        var id_num = $scope.id_num;
        var health_start = $scope.health_start;
        var health_end = $scope.health_end;
        var hlsj = new Date($scope.hlsj);
        var hlsc = $scope.hlsc;//新增的护理时长
        var jtsl = $scope.jtsl;
        var health_start = new Date($scope.health_start);
        var health_end = new Date($scope.health_end);

        //婚姻状况
        var obj = document.getElementsByName("hyzk");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                hy = obj[i].nextSibling.innerText;
            }
        }

        //生育状况
        obj = document.getElementsByName("syzk");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                sy = obj[i].nextSibling.innerText;
            }
        }

        //学历状况
        obj = document.getElementsByName("xlzk");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                xl = obj[i].nextSibling.innerText;
                break;
            }
        }

        //语言能力
        obj = document.getElementsByName("yynl");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                yy.push(obj[i].nextSibling.innerText);
            }
        }

        //已完成培训
        obj = document.getElementsByName("ywcpx");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                var temp;
                if (obj[i].checked) {
                    temp = obj[i].nextSibling;
                    if (temp.nodeName == "SPAN") {
                        ywcpx.push(obj[i].nextSibling.innerText);
                    }
                    else {
                        ywcpx.push(obj[i].nextSibling.value);
                    }
                }
            }
        }

        //跟岗医院
        obj = document.getElementsByName("ggyy");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                var temp;
                if (obj[i].checked) {
                    temp = obj[i].nextSibling;
                    if (temp.nodeName == "SPAN") {
                        ggyy.push(obj[i].nextSibling.innerText);
                    }
                    else {
                        ggyy.push(obj[i].nextSibling.value);
                    }
                }
            }
        }

        //护理双胞胎经验
        obj = document.getElementsByName("hljy");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                hljy = obj[i].nextSibling.innerText;
            }
        }
        var ihljy = 0;
        if (hljy == "有") {
            ihljy = 1;
        }

        //护理家庭类型
        obj = document.getElementsByName("hljt");
        for (var i = 0; i < obj.length; i++) {
            var temp;
            if (obj[i].checked) {
                temp = obj[i].nextSibling;
                if (temp.nodeName == "SPAN") {
                    hljt.push(obj[i].nextSibling.innerText);
                }
                else {
                    hljt.push(obj[i].nextSibling.value);
                }
            }
        }

        var head_img_url = $('img.head-img').attr('data-url');
        var intro_video_url = $('video.intro-video').attr('data-url');
        var id_front_img_url = $('img.front_id_img').attr('data-url');
        var id_back_img_url = $('img.back_id_img').attr('data-url');
        var health_img_url = $('img.health_img').attr('data-url');
        var skills = [];
        $('.certification-img').each(function () {
            var data_url = $(this).find('img.skill-img').attr('data-url');
            if (data_url != null) {
                var skill = {};
                skill.name = $(this).children('.certification-name').children('span').text();
                var rank_select = $(this).find('.rank-select');
                if (rank_select != null) {
                    skill.level = parseInt(rank_select.val());
                }
                skill.sn = $(this).find('.certification-num').val();
                skill.img_url = data_url;
                skills.push(skill);
            }
        });

        var working_photo = [];
        $('.material-img').each(function () {
            var url = $(this).find(".material-photo").attr('data-url');
            if (url != null) {
                var photo = {};
                photo.url = url;
                var tags = [];
                $(this).find(".tag-input").each(function () {
                    var tag = $(this).val();
                    tags.push(tag);
                });
                photo.tags = tags;
                working_photo.push(photo);
            }
        });

        // 判断必须信息是否输入
        if (age == null) {
            alert("请填写年龄");
            return;
        }

        if (name == null) {
            alert("请填写月嫂姓名");
            return;
        }

        if (phone == null) {
            alert("请填写月嫂的联系电话");
            return;
        }

        if (head_img_url == null) {
            alert("请上传头像");
            return;
        }

        if (intro_video_url == null) {
            alert("请上传介绍短片");
            return
        }

        if (emergency == null) {
            alert("请填写紧急联系人姓名");
            return;
        }

        if (relation == null) {
            alert("请填写与紧急联系人的关系");
            return;
        }

        if (contact == null) {
            alert("请填写紧急联系人的联系电话");
            return;
        }

        if ($scope.id_num == null || id_front_img_url == null || id_back_img_url == null) {
            alert("请补全身份证信息");
            return
        }

        if ($scope.health_start==null || $scope.health_end == null || health_img_url == null) {
            alert("请补全健康证信息");
            return;
        }

        if ($scope.city == null || $scope.province == null) {
            alert("请补全籍贯信息");
            return;
        }

        if ($scope.mLevel == null || $scope.mLevel < 1 || $scope.mLevel > 5) {
            alert("月嫂星级只能是1~5");
            return;
        }

        var birth_place = $scope.province.p + " " + $scope.city.n;

        // 判断输入合法性
        if (!/^\d{1,2}$/.test(age) || age > 80 || age < 18) {
            alert("请填写正确的年龄");
            return;
        }

        if (!/^\d{11}$/.test(phone)) {
            alert("请输入11位手机号码");
            return;
        }

        if (!/^\d{11}$/.test(contact)) {
            alert("请输入11位手机号码");
            return;
        }

        if (!/^\d{17}(\d|X)$/.test($scope.id_num)) {
            alert("请输入正确的身份证号");
            return;
        }

        if (!checkDate(health_start, health_end)) {
            alert("健康证有效期无效");
            return;
        }

        {
            var curTime = new Date().getTime();
            var hlsjTime = hlsj.getTime();
            if (curTime < hlsjTime) {
                alert("最早护理时间无效");
                return;
            }
        }

        if (!/^\d{1,2}$/.test(jtsl) || jtsl < 0 || jtsl > 99) {
            alert('护理家庭数量不合法，请输入0~99之间的数字');
            return;
        }

        if (name.length > 20) {
            alert("姓名过长，最多20字符");
            return;
        }
        if (emergency.length > 20) {
            alert("紧急联系人姓名过长，最多20字符");
            return;
        }
        if (relation.length > 20) {
            alert("与紧急联系人关系过长，最多20字符");
            return;
        }

        //通过http添加月嫂
        $http({

            method: "post",
            url: ip + "yuesao/detail",
            data: {
                uid: uid,
                access_token: access_token,
                name: name, // 月嫂姓名
                age: age, // 月嫂年龄
                birth_place: birth_place, // 月嫂籍贯
                phone_num: phone, // 月嫂联系电话
                head_img_url: head_img_url, // 月嫂头像地址
                intro_video_url: intro_video_url, // 月嫂介绍视频地址
                educationBackground: xl,
                emergencyContactPersonName: emergency,
                emergencyContactPersonRelation: relation,
                emergencyContactPersonNumber: contact,
                procreationState: sy,
                marriageState: hy,
                language: yy,
                level: $scope.mLevel,//页面中没有相关选项
                certificates: { // 证书资料
                    id_card: { // 身份证
                        num: $scope.id_num, // 证号
                        front_url: id_front_img_url, // 正面图片地址
                        back_url: id_back_img_url // 背面图片地址
                    },
                    health: { // 健康证
                        validity_period: { // 有效期
                            start: $scope.health_start, // 起始时间
                            end: $scope.health_end // 失效时间
                        },
                        img_url: health_img_url // 证件图片地址
                    },
                    skills: skills, // 技能证书
                    training: {
                        subjects: ywcpx, // 已完成培训项目，催乳师培训等
                        hospital: ggyy // 跟岗医院
                    },
                    nursing: {
                        first_time: $scope.hlsj, // 最早护理时间
                        experience: hlsc,//累计护理时长
                        num_of_family: jtsl, // 护理家庭数量
                        num_of_twins: ihljy, // 有无护理双胞胎经验
                        family_type: hljt,// 家庭类型 双外籍、单外籍等
                        location: []
                    },
                    working_photo: working_photo// 工作照链接地址
                }
            }
        }).success(function (data, status, headers, config) {
            if (data.success) {
                alert("添加成功！");
                window.close();
            }
            else {
                alert(data.msg);
                if(data.msg == "登录状态已失效,请重新登录"){
                    sessionStorage.clear();
                    $.cookie("access_token", null,{path:"/"});
                    location.href = "login.html";
                }
            }
        });
    };

    $scope.addIDImg = function () {
        document.getElementById('id1-img-input').click();
    };
    $scope.addHealthImg = function () {
        document.getElementById('health-img-input').click();
    };
    $scope.add_ywcpx = function () {
        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='ywcpx'/><input type='text' placeholder='自定义' style='width:120px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
        var template = angular.element(html);
        var dynamicYwcpxElement = $compile(template)($scope);
        var ywcpx_div = document.getElementById("ywcpx_div");
        angular.element(ywcpx_div).append(dynamicYwcpxElement);
    };

    $scope.add_ggyy = function () {
        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='ggyy'/><input type='text' placeholder='自定义' style='width:120px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
        var template = angular.element(html);
        var dynamicYwcpxElement = $compile(template)($scope);
        var ggyy_div = document.getElementById("ggyy_div");
        angular.element(ggyy_div).append(dynamicYwcpxElement);
    };

    $scope.add_hljt = function () {
        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='hljt'/><input type='text' placeholder='自定义' style='width:120px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
        var template = angular.element(html);
        var dynamicYwcpxElement = $compile(template)($scope);
        var hljt_div = document.getElementById("hljt_div");
        angular.element(hljt_div).append(dynamicYwcpxElement);
    }
});
//设置婚姻状况为单选
function hy_yh_select() {
    if (document.getElementById("hy_wh").checked) {
        document.getElementById("hy_wh").checked = false;
    }
}
function hy_wh_select() {
    if (document.getElementById("hy_yh").checked) {
        document.getElementById("hy_yh").checked = false;
    }
}

//设置生育为单选
function sy_yy_select() {
    if (document.getElementById("sy_wy").checked) {
        document.getElementById("sy_wy").checked = false;
    }
}
function sy_wy_select() {
    if (document.getElementById("sy_yy").checked) {
        document.getElementById("sy_yy").checked = false;
    }
}

//设置学历状况为单选
function xl_xx_select() {
    if (document.getElementById("xl_cz").checked) {
        document.getElementById("xl_cz").checked = false;
    }
    if (document.getElementById("xl_zz").checked) {
        document.getElementById("xl_zz").checked = false;
    }
    if (document.getElementById("xl_gz").checked) {
        document.getElementById("xl_gz").checked = false;
    }
    if (document.getElementById("xl_dz").checked) {
        document.getElementById("xl_dz").checked = false;
    }
}
function xl_cz_select() {
    if (document.getElementById("xl_xx").checked) {
        document.getElementById("xl_xx").checked = false;
    }
    if (document.getElementById("xl_zz").checked) {
        document.getElementById("xl_zz").checked = false;
    }
    if (document.getElementById("xl_gz").checked) {
        document.getElementById("xl_gz").checked = false;
    }
    if (document.getElementById("xl_dz").checked) {
        document.getElementById("xl_dz").checked = false;
    }
}
function xl_zz_select() {
    if (document.getElementById("xl_xx").checked) {
        document.getElementById("xl_xx").checked = false;
    }
    if (document.getElementById("xl_cz").checked) {
        document.getElementById("xl_cz").checked = false;
    }
    if (document.getElementById("xl_gz").checked) {
        document.getElementById("xl_gz").checked = false;
    }
    if (document.getElementById("xl_dz").checked) {
        document.getElementById("xl_dz").checked = false;
    }
}
function xl_gz_select() {
    if (document.getElementById("xl_xx").checked) {
        document.getElementById("xl_xx").checked = false;
    }
    if (document.getElementById("xl_cz").checked) {
        document.getElementById("xl_cz").checked = false;
    }
    if (document.getElementById("xl_zz").checked) {
        document.getElementById("xl_zz").checked = false;
    }
    if (document.getElementById("xl_dz").checked) {
        document.getElementById("xl_dz").checked = false;
    }
}
function xl_dz_select() {
    if (document.getElementById("xl_xx").checked) {
        document.getElementById("xl_xx").checked = false;
    }
    if (document.getElementById("xl_cz").checked) {
        document.getElementById("xl_cz").checked = false;
    }
    if (document.getElementById("xl_zz").checked) {
        document.getElementById("xl_zz").checked = false;
    }
    if (document.getElementById("xl_gz").checked) {
        document.getElementById("xl_gz").checked = false;
    }
}

//设置护理经验为单选
function hl_y_select() {
    if (document.getElementById("hl_w").checked) {
        document.getElementById("hl_w").checked = false;
    }
}
function hl_w_select() {
    if (document.getElementById("hl_y").checked) {
        document.getElementById("hl_y").checked = false;
    }
}

function addMaterialImg() {
    $('.material-photo').parent().removeAttr('data-on-uploaded');
    $('div.material-img:last').parent().append(addMaterialImgTemplate);
    bind_upload();
}

function checkDate(start, end) {
    var startTime = start.getTime();
    var endTime = end.getTime();
    var curTime = new Date().getTime();
    return startTime <= curTime && curTime <= endTime;
}
