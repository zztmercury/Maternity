package com.chml.controller;

import com.chml.util.OSSUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 16-9-6.
 */
@Controller
@RequestMapping("/api/media")
public class OSSController {

    private static Logger logger = LogManager.getRootLogger();
    @RequestMapping("getPostPolicy/{type}")
    @ResponseBody
    public String getPostPolicy(@PathVariable("type") String type, HttpServletRequest request) {
        logger.debug("getPostPolicy function");
        String bucket = "ejs-test";
        try {
            Properties properties = new Properties();
            InputStream in = getClass().getResourceAsStream("/config/oss.properties");
            properties.load(in);
            bucket = properties.getProperty("oss.bucket", bucket);
        } catch (IOException e) {
            //e.printStackTrace();
            logger.error("SomeThing is wrong:\n", e);
        }
        return OSSUtil.postObjectPolicy(bucket,30,50,type);
    }
}
