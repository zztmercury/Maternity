package com.chml.dto;

/**
 * Created by zzt on 16-10-6.
 */
public class GradePointDto {
    private int p;
    private int n;

    public GradePointDto(int p, int n) {
        this.p = p;
        this.n = n;
    }

    public int getP() {
        return p;
    }

    public void setP(int p) {
        this.p = p;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }
}
