package com.chml.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by zzt on 16-10-6.
 */
public class CertificateDto {
    private String name;
    private String number;
    private String level;
    @JsonProperty(value = "img_url")
    private String imgUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
