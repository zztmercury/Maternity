package com.chml.mapper;

import com.chml.pojo.TrainingExperience;
import com.chml.pojo.TrainingExperienceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TrainingExperienceMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table training_experience
     *
     * @mbggenerated
     */
    int countByExample(TrainingExperienceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table training_experience
     *
     * @mbggenerated
     */
    int deleteByExample(TrainingExperienceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table training_experience
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(Integer tid);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table training_experience
     *
     * @mbggenerated
     */
    int insert(TrainingExperience record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table training_experience
     *
     * @mbggenerated
     */
    int insertSelective(TrainingExperience record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table training_experience
     *
     * @mbggenerated
     */
    List<TrainingExperience> selectByExample(TrainingExperienceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table training_experience
     *
     * @mbggenerated
     */
    TrainingExperience selectByPrimaryKey(Integer tid);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table training_experience
     *
     * @mbggenerated
     */
    int updateByExampleSelective(@Param("record") TrainingExperience record, @Param("example") TrainingExperienceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table training_experience
     *
     * @mbggenerated
     */
    int updateByExample(@Param("record") TrainingExperience record, @Param("example") TrainingExperienceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table training_experience
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(TrainingExperience record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table training_experience
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(TrainingExperience record);

    int deleteByMid(int mid);
}