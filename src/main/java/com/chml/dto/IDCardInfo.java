package com.chml.dto;

/**
 * 月嫂身份证信息格式
 */
public class IDCardInfo {
    String num;
    String front_url;
    String back_url;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getFront_url() {
        return front_url;
    }

    public void setFront_url(String front_url) {
        this.front_url = front_url;
    }

    public String getBack_url() {
        return back_url;
    }

    public void setBack_url(String back_url) {
        this.back_url = back_url;
    }
}
