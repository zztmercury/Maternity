package com.chml.dto;

/**
 * 月嫂健康证信息格式
 */
public class HealthInfo {
    TimeBound validity_period;
    String img_url;

    public TimeBound getValidity_period() {
        return validity_period;
    }

    public void setValidity_period(TimeBound validity_period) {
        this.validity_period = validity_period;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }
}
