package com.chml.params;

import java.util.List;

/**
 * 删除月嫂时的传入数据格式
 */
public class DeleteMaternityParam {
    int uid;
    String access_token;
    List<Integer> id;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public List<Integer> getId() {
        return id;
    }

    public void setId(List<Integer> id) {
        this.id = id;
    }
}
